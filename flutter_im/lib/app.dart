import 'package:flutter/material.dart';
import './chat/message_page.dart';
import './contacts/contacts.dart';
import './personal/personal.dart';

//应用页面有状态
class App extends StatefulWidget {
  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
//当前选中页面索引
var _currentIndex = 0;

//聊天页面
MessagePage message;

//好友页面
Contacts contacts;

//我的页面
Personal person;

//根据当前索引返回不同页面
currentPage (){
  switch (_currentIndex) {
    case 0:
      if (message == null) {
        message = new MessagePage();
      }
      return message;
      break;
    case 1:
      if (contacts == null) {
        contacts = new Contacts();
      }
      return contacts;
      break;
    case 2:
      if (person == null) {
        person = new Personal();
      }
      return person;
      break;    
    default:
  }
}

//渲染某个菜单项，传入菜单标题，图片路径或图标
_popupMenuItem(String title,{String imagePath,IconData icon}){
  return PopupMenuItem(
    child: Row(
      children: <Widget>[
        //判断是使用图片路径还是图标
        imagePath != null 
        ? Image.asset(
          imagePath,
          width:32.0,
          height:32.0) 
        : SizedBox(
            width: 32.0,
            height:32.0,
            child: Icon(
            icon,
            color:Colors.white
            ),
          ),
        //显示菜单文本内容
        Container(
          padding: const EdgeInsets.only(left: 20.0),
          child: Text(
            title,
            style:TextStyle(color: Colors.white)
          ),
        )
      ],
    ),
  );
}

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text('即时通讯'),
        actions: <Widget>[
          GestureDetector(
            onTap: (){
              //跳转至搜索页
              Navigator.pushNamed(context, 'search');
            },
            child: Icon(
              Icons.search,
            ),
          ),
          Padding(
            //左右内边距
            padding: const EdgeInsets.only(left: 30.0,right: 20.0),
            child: GestureDetector(
              onTap: (){
                //弹出菜单
                showMenu(
                  context: context,
                  //定位在界面的由上角
                  position: RelativeRect.fromLTRB(500.0, 76.0, 10.0, 0.0),
                  //展示所有菜单
                  items: <PopupMenuEntry>[
                    _popupMenuItem('发起会话',imagePath: 'images/icon_menu_group.png'),
                    _popupMenuItem('添加好友',imagePath: 'images/icon_menu_addfriend.png'),
                    _popupMenuItem('联系客服',icon: Icons.person),
                  ],
                );
              },
              //菜单按钮
              child: Icon(Icons.add),
            ),
          )
        ],),
      
      //底部导航按钮

      bottomNavigationBar: BottomNavigationBar(
        //通过fixedColor 设置选中item的颜色
        type: BottomNavigationBarType.fixed,
        //当前页面索引
        currentIndex: _currentIndex,
        //按下后设置当前页面索引
        onTap: ((index){
          setState(() {
            _currentIndex = index;
          });
        }),

        //底部导航按钮
        items: [
          //导航按钮项传入文本及图标
          BottomNavigationBarItem(
            title: Text(
              '聊天',
              style:TextStyle(
                color: _currentIndex == 0 ? Color(0xFF46c01b) : Color(0xff999999),
              ),
            ),
            //判断当前索引图标切换
            icon: _currentIndex == 0 
            ? Image.asset('images/message_pressed.png',width: 32.0,height:28.0,color: Color(0xFF46c01b),) 
            : Image.asset('images/message_normal.png',width: 32.0,height: 28.0,color: Color(0xff999999),),
          ),
          
          BottomNavigationBarItem(
            title: Text(
              '好友',
              style:TextStyle(
                color: _currentIndex == 1 ? Color(0xFF46c01b) : Color(0xff999999),
              ),
            ),
            //判断当前索引图标切换
            icon: _currentIndex == 1 
            ? Image.asset('images/contact_list_pressed.png',width: 32.0,height:28.0,color: Color(0xFF46c01b),) 
            : Image.asset('images/contact_list_normal.png',width: 32.0,height: 28.0,color: Color(0xff999999),),
          ),

          BottomNavigationBarItem(
            title: Text(
              '我的',
              style:TextStyle(
                color: _currentIndex == 2 ? Color(0xFF46c01b) : Color(0xff999999),
              ),
            ),
            //判断当前索引图标切换
            icon: _currentIndex == 2 
            ? Image.asset('images/profile_pressed.png',width: 32.0,height:28.0,color:Color(0xFF46c01b)) 
            : Image.asset('images/profile_normal.png',width: 32.0,height: 28.0,color:Color(0xff999999)),
          ),

        ],
      ),
      //中间显示当前页面
      body: currentPage(),
    );
  }
}

/*
class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'App',
      debugShowCheckedModeBanner: false,
      theme: mDefaultTheme,
      home: LayoutUI(),
    );
  }
}

class LayoutUI extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('App'),),
      body: Center(
        // child: Image.network('http://pic1.win4000.com/mobile/2019-11-28/5ddf2e6792dd6.jpg',fit: BoxFit.cover,),
        child: Image.asset('images/4.jpg'),
      ),
    );
  }
}
final ThemeData mDefaultTheme = ThemeData(
  primaryColor: Colors.green,
  scaffoldBackgroundColor: Color(0xFFebebeb),
  cardColor: Colors.green,
);

*/