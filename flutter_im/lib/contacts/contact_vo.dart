import 'package:flutter/cupertino.dart';

class ContactVO{
  //字母排列值
  String seationKey;
  //名称
  String name;
  //头像url
  String avatarUrl;
  //构造函数
  ContactVO({@required this.seationKey,this.name,this.avatarUrl});
}

List<ContactVO> contactData = [
  ContactVO(
    seationKey: 'A',
    name: 'A张三',
    avatarUrl: 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1576086737376&di=b3ec86107c776cdb3893bf55ed9589c5&imgtype=0&src=http%3A%2F%2Fimg.duoziwang.com%2F2018%2F13%2F03071900000792.jpg',
  ),

  ContactVO(
    seationKey: 'A',
    name: '阿黄',
    avatarUrl: 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1576086737376&di=3144b8c7e61e1215c8653cdab82429d8&imgtype=0&src=http%3A%2F%2Fp1.ssl.cdn.btime.com%2Ft01e0049449fc44df09.jpg%3Fsize%3D400x400',
  ),

  ContactVO(
    seationKey: 'B',
    name: '波波',
    avatarUrl: 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1576086883921&di=692cd446668b60a0710071a7ecbd9513&imgtype=jpg&src=http%3A%2F%2Fimg.qqzhi.com%2Fuploads%2F2018-11-29%2F050544659.jpg',
  ),

];