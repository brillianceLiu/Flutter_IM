import 'package:flutter/material.dart';
import './contact_vo.dart';

//好友列表

class ContactItem extends StatelessWidget {
  //好友数据VO
  final ContactVO item;
  //标题名
  final String titleName;
  //图片路径
  final String imageName;
  //构建方法
  ContactItem({this.item,this.titleName,this.imageName});

  //渲染好友列表
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border(bottom: BorderSide(width: 0.5,color: Color(0xFFd9d9d9))),
      ),
      height: 52.0,
      child: FlatButton(
        onPressed: (){},
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            //展示头像或图片
            imageName == null 
            ? Image.network(
              item.avatarUrl!='' 
                  ? item.avatarUrl
                  : 
                  'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1576086737375&di=848dddd22ae739215769d609bf569eae&imgtype=0&src=http%3A%2F%2Fwww.17qq.com%2Fimg_qqtouxiang%2F76984448.jpeg',
                  width: 36.0,
                  height: 36.0,
                  scale: 0.9,)
                  : Image.asset(imageName,width:36.0,height:36.0
                  ),
            Container(
              margin: const EdgeInsets.only(left: 12.0),
              child: Text(
                titleName == null ? item.name ?? '暂时' : titleName,
                style:TextStyle(fontSize: 18.0,color:Color(0xFF353535)),
                maxLines: 1,
              ),
            ) ,     
          ],
        ),
      ),
    );
  }
}