
//消息类型枚举类型
enum MessageType {SYSTEM,PUBLIC,CHAT,GRUOUP}

//聊天数据
class MessageData{
  //头像
  String avatar;
  //主题标
  String title;
  //子标题
  String subTitle;
  //消息时间
  DateTime time;
  //消息类型
  MessageType type;

  MessageData(this.avatar,this.title,this.subTitle,this.time,this.type);
}


List <MessageData> messageData = [
  MessageData(
     'http://pic4.zhimg.com/50/v2-208e5832a11b3f6e22631edfafa3c450_hd.jpg',
     '一休哥',
     '突然想到的',
     DateTime.now(),
     MessageType.CHAT
     ),
  MessageData(
     'http://b-ssl.duitang.com/uploads/item/201703/06/20170306012959_aH4Ki.jpeg',
     '多拉爱梦',
     '机器猫',
      DateTime.now(),
      MessageType.CHAT
    ),
    MessageData(
     'http://b-ssl.duitang.com/uploads/item/201607/26/20160726185736_yPmrE.thumb.224_0.jpeg',
     '麦子',
     '我好想你',
     DateTime.now(),
     MessageType.CHAT
     ),
  MessageData(
     'http://cdn.duitang.com/uploads/item/201411/30/20141130222349_SnjCy.thumb.700_0.jpeg',
     '大老婆',
     '下班了没？',
      DateTime.now(),
      MessageType.CHAT
    ),
    MessageData(
     'http://b-ssl.duitang.com/uploads/item/201703/30/20170330170642_Q3v2E.jpeg',
     '二老婆',
     '一个人睡觉好害怕，你赶紧回来',
     DateTime.now(),
     MessageType.CHAT
     ),
  MessageData(
     'http://b-ssl.duitang.com/uploads/item/201809/26/20180926162125_vjbwi.jpg',
     '三老婆',
     '好想你',
      DateTime.now(),
      MessageType.CHAT
    ),
    MessageData(
     'http://pic1.zhimg.com/50/v2-90be1ea8a21a1974eb5334dc16036c63_hd.jpg',
     '四老婆',
     '周末出去玩吧',
     DateTime.now(),
     MessageType.CHAT
     ),
  MessageData(
     'http://tupian.qqjay.com/tou2/2018/1210/d0292c5e95603cfc36ca1d8610e60841.jpg',
     '五老婆',
     '我们去看胡歌主演的《南方车站的聚会》吧',
      DateTime.now(),
      MessageType.CHAT
    ),
    MessageData(
     'http://pic.qqtn.com/up/2019-11/2019110618002154560.jpg',
     '六老婆',
     '我新看上个包，你帮我买吧',
     DateTime.now(),
     MessageType.CHAT
     ),
  MessageData(
     'http://tupian.qqjay.com/tou2/2018/1217/52d5550ca0fd49b8d60b7222c06fe814.jpg',
     'lover',
     '这周给我回家看看我妈吧',
      DateTime.now(),
      MessageType.CHAT
    ),


];